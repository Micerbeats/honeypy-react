// react imports
import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

// style imports
import './App.scss';

// container imports
import Header from './Header/Header';
import Directory from './Directory/Directory';
import Main from './Main/Main';
import Toolbar from './Toolbar/Toolbar';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className='app'>

          <Header className='app__header'/>

          <div className='app__container'>
            <Directory className='app__directory'/>
            <Main className='app__main'/>
          </div>

          <Toolbar />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
