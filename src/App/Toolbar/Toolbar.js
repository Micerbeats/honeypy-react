import React, {Component} from 'react';
import './Toolbar.scss';
import {
  Route,
  NavLink,
  Switch
} from 'react-router-dom';

class Toolbar extends Component {

  state = {
    display:'expanded'
  };

  componentDidMount() {
    console.log(this.props);
  }

  render() {
    return (
        <div className={'toolbar toolbar__' + this.state.display}>

          Toolbar

        </div>
    );
  }
};

export default Toolbar;
